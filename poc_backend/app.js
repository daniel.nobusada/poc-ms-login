const express = require('express')
const bodyParser = require('body-parser')
var cors = require('cors')
const app = express()
const port = 3001

app.use(bodyParser.urlencoded());
app.use(bodyParser.json());
app.use(cors())

var userList = []

const availableUsers = [
    {
        userId: 1,
        userName: 'William Shakespeare',
        userImage: 'https://s2.glbimg.com/nnECR-eUryPx0VVLsC3E0JGYKVo=/e.glbimg.com/og/ed/f/original/2018/02/09/shakespeare_comedy_list_main.jpg'
    },
    {
        userId: 2,
        userName: 'Machado de Assis',
        userImage: 'https://www.jota.info/wp-content/uploads/2020/04/a-lideranca-de-machado-de-assis-1-e1586841124864.jpg'
    },
    {
        userId: 3,
        userName: 'Jose Saramago',
        userImage: 'https://www.portaldaliteratura.com/assets/files_autores/58.jpg'
    },
]

const dropInactiveUsers = () => {
    userList = userList
        .filter(el => Date.now() - el.lastCheckIn < 20000)
}

app.get('/', (req, res) => {
  res.sendStatus(200)
})

app.post('/login', (req, res) => {
    console.log('login being executed with req: ' + JSON.stringify(req.body))
    if(availableUsers.filter(el => el.userId == req.body.userId).length > 0) {
        if(userList.filter(el => el.user.userId == req.body.userId) == 0) {
            res.sendStatus(200)
        }
        else res.sendStatus(403)
    }
    else res.sendStatus(404)
})

app.post('/join', (req, res) => {
    console.log('join being executed')
    if(req.body.userId !== null) {
        if(userList.filter(el => el.userId == req.body.userId).length > 0) {
            res.sendStatus(403)
        }
        else {
            const user = availableUsers.filter(el => el.userId == req.body.userId)
            userList.push({
                'user': user,
                lastCheckIn: Date.now()
            })
            res.sendStatus(200)
        }
    }
    else res.sendStatus(404)
})

app.post('/keepalive', (req, res) => {
    console.log('keepalive being executed')
    if (req.body.userId !== null) {
        userList
            .filter(el => el.user[0].userId == req.body.userId)
            .map(el => el.lastCheckIn = Date.now())
        res.sendStatus(200)
    }
    else {
        res.sendStatus(404)
    }
})

app.get("/retrieveUserList", (req, res) => {
    res.send({'onlineList' : userList})
})

app.listen(port, () => {
    setInterval(() => {
        console.log("userlist: " + JSON.stringify(userList))
        dropInactiveUsers()
    }, 5000);
})