import React, {Component} from 'react'
import OnlineList from './onlineList'
import officeImage from './../assets/office.jpg'

var imageStyle = {
    width: '100%',
    height: 'auto'
}

var activeUsersList = []

class Office extends Component {
    constructor(props) {
        super(props)

        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(event) {


        this.props.updateUserListUpdater(event.target.value)
    }

    componentDidMount() {
        const requestOptions = {
            method: 'POST', 
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(this.state)
        }
        fetch('http://localhost:3001/join', requestOptions)
            .then()
            .catch(function(error) {console.log(error)})
        //chamar pra adicionar o cara

        fetch('http://localhost:3001/retrieveUserList')
            .then()
    }

    // function pra dar loop de keepalive
    // function dando fetch da lista

    render() {
        return (
            <div>
                <img src={officeImage} style={imageStyle} />
                <OnlineList list={activeUsersList} />
            </div>
        )
    }
}

export default Office