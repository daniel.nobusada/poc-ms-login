import React, { Component } from 'react'

class Login extends Component {
    constructor(props) {
        super(props)

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(event) {
        this.props.userUpdater(event.target.value)
    }

    handleSubmit(event) {
        const requestOptions = {
            method: 'POST', 
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({'userId': this.props.userId})
        }
        console.log("state: " + JSON.stringify(this.props.userId))
        fetch('http://localhost:3001/login', requestOptions)
            .then( function() {
                setTimeout(() => {
                    window.location.replace("http://localhost:3000/office") 
                }, 500);
            })
            .catch(function(error) {console.log(error)})
        
        event.preventDefault()
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        userId:
                        <input type="text" value={this.props.userId} onChange={this.handleChange} />
                    </label>
                    <input type="submit" value="Enviar" />
                </form>
            </div>
        )
    }
}

export default Login