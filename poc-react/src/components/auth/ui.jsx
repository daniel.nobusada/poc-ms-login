import React from "react";
import Button from "react-bootstrap/Button";
import { AuthenticatedTemplate, UnauthenticatedTemplate, useMsal } from "@azure/msal-react";
import { loginRequest } from "./authConfig";

const SignInSignOutButton = () => {
    const { instance } = useMsal();
    return (
        <>
            <AuthenticatedTemplate>
                <Button variant="secondary" onClick={() => instance.logout()} className="ml-auto">Sign Out</Button>
            </AuthenticatedTemplate>
            <UnauthenticatedTemplate>
                <Button variant="secondary" onClick={() => instance.loginRedirect(loginRequest)}>Sign in</Button>
            </UnauthenticatedTemplate>
        </>
    );
};
export const PageLayout = (props) => {
    return (
        <>
            <SignInSignOutButton/>
            {props.children}
        </>
    );
};