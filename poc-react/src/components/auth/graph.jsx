
import React from "react";
import { graphConfig } from "./authConfig";

export async function callMsGraph(accessToken) {
    const headers = new Headers();
    const bearer = `Bearer ${accessToken}`;

    headers.append("Authorization", bearer);

    const options = {
        method: "GET",
        headers: headers
    };

    return fetch(graphConfig.graphMeEndpoint, options)
        .then(response => {
            console.log("--- " + JSON.stringify(response.clone().json())) ;
            return response.json()
        })
        .catch(error => console.log(error));
    // console.log('request made to Graph API at: ' + new Date().toString());

    // fetch(graphConfig.graphMeEndpoint, options)
    //     .then(response => response.json())
    //     .then(response => callback(response, graphConfig.graphMeEndpoint))
    //     .catch(error => console.log(error));
}

export const ProfileData = (props) => {
    return (
        <div id="profile-div">
            <p><strong>Title: </strong> {props.graphData.jobTitle}</p>
    <pre> {props.graphData.username} </pre>
        </div>
    );
};