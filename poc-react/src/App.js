import './App.css';
import Login from './components/login';
import Office from './components/office';
import { Switch, Route } from 'react-router-dom'
import React, {Component} from 'react'
import Auth from './components/auth/auth';

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {userId: '', activeUserList: ''}
  }

  userUpdater = (userId) => {
    this.setState({userId: userId})
  }

  updateUserListUpdater = (newList) => {
    this.setState({activeUserList: newList})
  }

  render() {
    return (
      <main>
        <Switch>
        <Route
          path="/"
          exact
          //render={props => <Login  userId={this.state.userId} userUpdater={this.userUpdater} />}
          render={props => <Auth />}
        />
        <Route
          path="/office"
          exact
          render={props => <Office userId={this.state.userId} activeUserList={this.state.activeUserList} activeUserListUpdater={this.updateUserListUpdater} />}  
        />
        </Switch>
      </main>
    );
  }
}

export default App;
